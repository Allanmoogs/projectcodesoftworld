<?php
namespace App\Http\Controllers;

use App\Models\Company;
use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;

class CompanyAPIController extends Controller
{
    protected $user;
 
    public function __construct()
    {
        $this->user = JWTAuth::parseToken()->authenticate();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->user
            ->companies()
            ->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validate data
        $data = $request->only('name', 'email', 'website');
        $validator = Validator::make($data, [
            'name' => 'required|string',
            'email' => 'required|string',
            'website' => 'required|string',
        ]);

        //Send failed response if request is not valid
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()], 400);
        }

        //Request is valid, create new company
        $company = $this->user->companies()->create([
            'name' => $request->name,
            'email' => $request->email,
            'website' => $request->website
        ]);

        //Company created, return success response
        return response()->json([
            'success' => true,
            'message' => 'Company created successfully',
            'data' => $company
        ], Response::HTTP_OK);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $company = $this->user->companies()->find($id);
    
        if (!$company) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, company not found.'
            ], 400);
        }
    
        return $company;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Company $company)
    {
        //Validate data
        $data = $request->only('name', 'email', 'website');
        $validator = Validator::make($data, [
            'name' => 'required|string',
            'email' => 'required|string',
            'website' => 'required|string'
        ]);

        //Send failed response if request is not valid
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()], 400);
        }

        //Request is valid, update company
        $company = $company->update([
            'name' => $request->name,
            'email' => $request->email,
            'website' => $request->website
        ]);

        //Comapny updated, return success response
        return response()->json([
            'success' => true,
            'message' => 'Company updated successfully',
            'data' => $company
        ], Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        $company->delete();
        
        return response()->json([
            'success' => true,
            'message' => 'Company deleted successfully'
        ], Response::HTTP_OK);
    }
}