<?php
namespace App\Http\Controllers;

use App\Models\Employee;
use App\Models\Company;
use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;

class EmployeeAPIController extends Controller
{
    protected $user;
 
    public function __construct()
    {
        $this->user = JWTAuth::parseToken()->authenticate();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->user
            ->employees()
            ->get();
        foreach ($data as $key => $value) {
            $value->company_name = Company::find($value->company_id)->name;
        }
        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validate data
        $data = $request->only('first_name', 'last_name', 'email', 'phone_number', 'company_id');
        $validator = Validator::make($data, [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'email' => 'required|string',
            'phone_number' => 'required|string',
            'company_id' => 'required|integer|exists:companies,id'
        ]);

        //Send failed response if request is not valid
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()], 400);
        }

        //Request is valid, create new employee
        $employee = $this->user->employees()->create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'phone_number' => $request->phone_number,
            'company_id' => $request->company_id
        ]);

        $employee->company_name = Company::find($employee->company_id)->name;
        //Employee created, return success response
        return response()->json([
            'success' => true,
            'message' => 'Employee created successfully',
            'data' => $employee
        ], Response::HTTP_OK);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $employee = $this->user->employees()->find($id);
        $employee->company_name = Company::find($employee->company_id)->name;
        if (!$employee) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, employee not found.'
            ], 400);
        }
    
        return $employee;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        //
    }

    /**
         * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee)
    {
        //Validate data
        $data = $request->only('first_name', 'last_name', 'email', 'phone_number', 'company_id');
        $validator = Validator::make($data, [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'email' => 'required|string',
            'phone_number' => 'required|string',
            'company_id' => 'required|integer|exists:companies,id'
        ]);

        //Send failed response if request is not valid
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()], 200);
        }
        
        $emp = $employee;
        //Request is valid, update employee
        $employee = $employee->update([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'phone_number' => $request->phone_number,
            'company_id' => $request->company_id
        ]);
        $new_emp = Employee::find($emp->id);
        $new_emp->company_name = Company::find($new_emp->company_id)->name;

        //Employee updated, return success response
        return response()->json([
            'success' => true,
            'message' => 'Employee updated successfully',
            'data' => $new_emp
        ], Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        $employee->delete();
        
        return response()->json([
            'success' => true,
            'message' => 'Employee deleted successfully'
        ], Response::HTTP_OK);
    }
}