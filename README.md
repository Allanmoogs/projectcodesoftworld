

## Installation
    Clone the repo by running the command - git clone https://gitlab.com/Allanmoogs/projectcodesoftworld.git

    Run the command - composer update

    Set up your database name and password in the .env file

    Run the command - php artisan key:generate

    Run the command - npm install

    Run the command - npm run dev

    Run the command - php artisan migrate

    Run the command - php artisan db:seed

    Now your are good to go, You can start testing locally by running the command - php artisan serve

    Open the link the browser and start testing the UI, Login using the credentials Email: admin@admin.com Password: password

## API Documentation
    Below are the different API endpoints, their menthods. Locally you can use http://127.0.0.1:8000/ and attach the endpoints below
    Note: use x-www-form-urlencoded forms while testing endpoints for PUT and DELETE methods since laravel doesn't support other basic form-data

    Post - api/login

    Post - api/register

    GET - api/logout

    GET - api/get_user

    GET - api/companies

    GET - api/companies/{id}

    POST - api/company/create

    PUT - api/company/update/{id}

    DELETE - api/company/delete/{id}

    GET - api/employees

    GET - api/employees/{id}

    POST - api/employee/create

    PUT - api/employee/update/{id}

    DELETE - api/employee/delete/{id}


    All the best. Thanks for your time
    
## API Testing and Web View ScreenShots

![](https://gitlab.com/Allanmoogs/projectcodesoftworld/-/raw/master/public/screenshots/Screenshot%20(47).png)
![](https://gitlab.com/Allanmoogs/projectcodesoftworld/-/raw/master/public/screenshots/Screenshot%20(48).png)
![](https://gitlab.com/Allanmoogs/projectcodesoftworld/-/raw/master/public/screenshots/Screenshot%20(49).png)
![](https://gitlab.com/Allanmoogs/projectcodesoftworld/-/raw/master/public/screenshots/Screenshot%20(50).png)
![](https://gitlab.com/Allanmoogs/projectcodesoftworld/-/raw/master/public/screenshots/Screenshot%20(51).png)
![](https://gitlab.com/Allanmoogs/projectcodesoftworld/-/raw/master/public/screenshots/Screenshot%20(52).png)
![](https://gitlab.com/Allanmoogs/projectcodesoftworld/-/raw/master/public/screenshots/Screenshot%20(53).png)
![](https://gitlab.com/Allanmoogs/projectcodesoftworld/-/raw/master/public/screenshots/Screenshot%20(54).png)
![](https://gitlab.com/Allanmoogs/projectcodesoftworld/-/raw/master/public/screenshots/Screenshot%20(55).png)
![](https://gitlab.com/Allanmoogs/projectcodesoftworld/-/raw/master/public/screenshots/Screenshot%20(56).png)
![](https://gitlab.com/Allanmoogs/projectcodesoftworld/-/raw/master/public/screenshots/Screenshot%20(57).png)
![](https://gitlab.com/Allanmoogs/projectcodesoftworld/-/raw/master/public/screenshots/Screenshot%20(58).png)
![](https://gitlab.com/Allanmoogs/projectcodesoftworld/-/raw/master/public/screenshots/Screenshot%20(59).png)
![](https://gitlab.com/Allanmoogs/projectcodesoftworld/-/raw/master/public/screenshots/Screenshot%20(60).png)
![](https://gitlab.com/Allanmoogs/projectcodesoftworld/-/raw/master/public/screenshots/Screenshot%20(61).png)
![](https://gitlab.com/Allanmoogs/projectcodesoftworld/-/raw/master/public/screenshots/Screenshot%20(62).png)
