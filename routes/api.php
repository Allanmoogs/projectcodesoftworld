<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApiController;
use App\Http\Controllers\CompanyAPIController;
use App\Http\Controllers\EmployeeAPIController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('login', [ApiController::class, 'authenticate']);
Route::post('register', [ApiController::class, 'register']);

Route::group(['middleware' => ['jwt.verify']], function() {
    Route::get('logout', [ApiController::class, 'logout']);
    Route::get('get_user', [ApiController::class, 'get_user']);
    //compny api endpoint
    Route::get('companies', [CompanyAPIController::class, 'index']);
    Route::get('companies/{id}', [CompanyAPIController::class, 'show']);
    Route::post('company/create', [CompanyAPIController::class, 'store']);
    Route::put('company/update/{company}',  [CompanyAPIController::class, 'update']);
    Route::delete('company/delete/{company}',  [CompanyAPIController::class, 'destroy']);
    //employee api endpoint
    Route::get('employees', [EmployeeAPIController::class, 'index']);
    Route::get('employees/{id}', [EmployeeAPIController::class, 'show']);
    Route::post('employee/create', [EmployeeAPIController::class, 'store']);
    Route::put('employee/update/{employee}',  [EmployeeAPIController::class, 'update']);
    Route::delete('employee/delete/{employee}',  [EmployeeAPIController::class, 'destroy']);
});



